// ==UserScript==
// @name        Exclude pick-up only offers
// @namespace   de.bighead.ebay.pickup
// @include      http://*.ebay.*/*
// @include      http://ebay.com/*
// @version     1
// ==/UserScript==

/*global window, unsafeWindow, document, GM_log */

"use strict";

function showWarning(removedCount) {
   var resultSet = document.getElementById("ResultSetItems"),
       warn = document.createElement("p"),
       warnText = document.createTextNode("Removed " + removedCount + " pick-up only offers.");

   warn.appendChild(warnText);
   warn.style.color = "red";

   resultSet.parentNode.insertBefore(warn, resultSet);
}

function go() {
   var ships = document.getElementsByClassName("ship"),
       isOnlyLocalPickup = new RegExp("Nur Abholung"),
       i = 0,
       element = null,
       rowTable = null,
       nRemoved = 0;

   for (i = 0; i < ships.length; i++) {
      element = ships.item(i);

      if (element.tagName.toLowerCase() === "span") {

         if (isOnlyLocalPickup.exec(element.innerHTML) !== null) {
            rowTable = element;

            do {
               rowTable = rowTable.parentNode;
            } while (rowTable !== null && rowTable.tagName.toLowerCase() !== "table");


            if (rowTable !== null) {
               rowTable.style.display = "none";
               nRemoved++;
            }
         }
      }
   }

   console.log("Removed " + nRemoved + " rows.");
   showWarning(nRemoved);
}
   

document.addEventListener("DOMContentLoaded", go, false);


